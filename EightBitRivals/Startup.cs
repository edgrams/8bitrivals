﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EightBitRivals.Startup))]
namespace EightBitRivals
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
