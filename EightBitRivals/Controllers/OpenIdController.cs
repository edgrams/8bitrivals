﻿using System;
using System.Web.Mvc;
using EightBitRivals.Domain.Exceptions;
using EightBitRivals.Domain.Interfaces;
using EightBitRivals.Domain.Resources;

namespace EightBitRivals.Controllers
{
    public class OpenIdController : Controller
    {
        private readonly IAuthenticationService authenticationService;
        private readonly IOpenIdService openIdService;
        private readonly IUserService userService;
        private readonly IOpenIdAuthInfoFactory openIdAuthInfoFactory;

        internal const string ProviderNameKey = "providerName";
        internal const string RedirectUrlKey = "redirectUrl";
        internal const string StateKey = "state";

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenIdController" /> class.
        /// </summary>
        /// <param name="authenticationService">The authentication service.</param>
        /// <param name="openIdService">The open identifier service.</param>
        /// <param name="userService">The user service.</param>
        /// <param name="openIdAuthInfoFactory">The open identifier authentication information factory.</param>
        public OpenIdController(IAuthenticationService authenticationService, IOpenIdService openIdService,
            IUserService userService, IOpenIdAuthInfoFactory openIdAuthInfoFactory)
        {
            this.authenticationService = authenticationService;
            this.openIdService = openIdService;
            this.userService = userService;
            this.openIdAuthInfoFactory = openIdAuthInfoFactory;
        }

        /// <summary>
        /// Logins this instance.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {  
            var providers = this.openIdService.GetProviders();
            return this.View(providers);
        }

        /// <summary>
        /// Logins the specified provider name.
        /// </summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">providerName</exception>
        [HttpGet]
        public ActionResult Login(string providerName)
        {
            if (string.IsNullOrEmpty(providerName))
            {
                throw new ArgumentNullException("providerName");    
            }
            Session[ProviderNameKey] = providerName;

            var state = this.openIdService.GetAntiForgeryToken();
            Session[StateKey] = state;

            var provider = this.openIdService.GetProvider(providerName);
            var loginUrl = provider.GetLoginUrl(state);
            return this.Redirect(loginUrl);
        }

        /// <summary>
        /// Logins the callback.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        /// state
        /// or
        /// code
        /// </exception>
        [HttpGet]
        public ActionResult LoginCallback(string state, string code)
        {
            if (string.IsNullOrEmpty(state))
            {
                throw new ArgumentNullException("state");
            }

            if (string.IsNullOrEmpty(code))
            {
                throw new ArgumentNullException("code");
            }

            this.ValidateStateFromSession(state);

            var providerName = this.GetProviderNameFromSession();
            var authInfo = this.openIdAuthInfoFactory.Build(providerName, code);
            var user = this.userService.GetUser(providerName, authInfo.SecurityToken.Subject);
            this.authenticationService.SignIn(this.HttpContext, authInfo, user);

            return this.RedirectFromSession();
        }

        internal ActionResult RedirectFromSession()
        {
            var redirectUrl = Session[RedirectUrlKey] as string;
            if (redirectUrl != null && this.Url.IsLocalUrl(redirectUrl))
            {
                return this.Redirect(redirectUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        // ReSharper disable once UnusedParameter.Local
        private void ValidateStateFromSession(string state)
        {
            var antiForgeryToken = Session[StateKey] as string;
            if (string.IsNullOrEmpty(antiForgeryToken))
            {
                throw new EightBitRivalsException(ExceptionMessages.StateNotSavedFromSession);
            }

            if (antiForgeryToken != state)
            {
                throw new EightBitRivalsException(ExceptionMessages.StateValidationMismatch);
            }
        }

        private string GetProviderNameFromSession()
        {
            var providerName = Session[ProviderNameKey] as string;
            if (string.IsNullOrEmpty(providerName))
            {
                throw new EightBitRivalsException(ExceptionMessages.ProviderNameNotSavedFromSession);
            }
            return providerName;
        }
    }
}