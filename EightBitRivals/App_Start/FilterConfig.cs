﻿using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;

namespace EightBitRivals
{
    [ExcludeFromCodeCoverage]
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
