﻿using EightBitRivals.Domain.Exceptions;
using EightBitRivals.Domain.Interfaces;
using EightBitRivals.Domain.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TSBDomain.Models;

namespace EightBitRivals.Areas.TSB.Controllers
{
    public class UploadController : Controller
    {
        private IGameService<Game> gameService;
        private IExtractor<Game> extractor;

        /// <summary>
        /// Initializes a new instance of the <see cref="UploadController"/> class.
        /// </summary>
        /// <param name="gameService">The game service.</param>
        /// <param name="extractor">The extractor.</param>
        public UploadController(IGameService<Game> gameService, IExtractor<Game> extractor)
        {
            this.gameService = gameService;
            this.extractor = extractor;
        }

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Indexes the specified files.
        /// </summary>
        /// <param name="files">The files.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">files</exception>
        [HttpPost]
        public ActionResult Index(List<HttpPostedFileBase> files)
        {
            if (files == null)
            {
                throw new ArgumentNullException("files");
            }

            if (files.Count() != 1)
            {
                throw new EightBitRivalsException(ExceptionMessages.UploadOneSaveState);
            }

            var saveState = files.First();
            var game = this.extractor.Extract(saveState);
            this.gameService.Add(game, 1, 2, true);

            return View("Upload", null, Messages.UploadSuccessful);
        }
    }
}