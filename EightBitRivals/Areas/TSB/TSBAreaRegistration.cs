﻿using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;

namespace EightBitRivals.Areas.TSB
{
    [ExcludeFromCodeCoverage]
    public class TSBAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "TSB";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "TSB_default",
                "TSB/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}