﻿using EightBitRivals.Domain.Extractors.TSB;
using EightBitRivals.Domain.Factories;
using EightBitRivals.Domain.HttpClients;
using EightBitRivals.Domain.Interfaces;
using EightBitRivals.Domain.Services;
using EightBitRivals.Domain.Services.TSB;
using EightBitRivals.Domain.Settings;
using Ninject.Modules;
using System.Diagnostics.CodeAnalysis;
using TSBDomain.Models;

namespace EightBitRivals.Ioc
{
    [ExcludeFromCodeCoverage]
    class EightBitRivalsModule : NinjectModule
    {
        /// <summary>
        /// Loads the module into the kernel.
        /// </summary>
        public override void Load()
        {
            this.LoadExtractors();
            this.LoadFactories();
            this.LoadHttpClients();
            this.LoadServices();
            this.LoadSettings();
        }

        private void LoadExtractors()
        {
            Bind<IExtractor<Game>>().To<TSBExtractor>();
        }

        private void LoadHttpClients()
        {
            Bind<IHttpClient>().To<WebHttpClient>();
            Bind(typeof(IWebApiClient<>)).To(typeof(WebApiClient<>));
        }

        private void LoadFactories()
        {
            Bind<IOpenIdAuthInfoFactory>().To<OpenIdAuthInfoFactory>();
        }

        private void LoadServices()
        {
            Bind<IUserService>().To<UserService>();
            Bind<IAuthenticationService>().To<AuthenticationService>();
            Bind<IGameService<Game>>().To<TSBGameService>();
            Bind<IOpenIdService>().To<OpenIdService>();
        }

        private void LoadSettings()
        {
            Bind<IEightBitRivalsSettings>().To<EightBitRivalsSettings>().InSingletonScope();
        }
    }
}