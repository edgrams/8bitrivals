﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using TSBDomain.Models;
using TSBDomain.Models.Player.Stats;
using TSBDomain.Models.Team.Stats;

namespace EightBitRivals.Rest.Contexts
{
    public class GameContext : DbContext
    {
        /// <summary>
        /// Gets or sets the games.
        /// </summary>
        /// <value>
        /// The games.
        /// </value>
        public DbSet<Game> Games { get; set; }

        /// <summary>
        /// Gets or sets the player stats.
        /// </summary>
        /// <value>
        /// The player stats.
        /// </value>
        public DbSet<PlayerStats> PlayerStats { get; set; }

        /// <summary>
        /// Gets or sets the team stats.
        /// </summary>
        /// <value>
        /// The team stats.
        /// </value>
        public DbSet<TeamStats> TeamStats { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameContext"/> class.
        /// </summary>
        public GameContext() : base("TSBRivalDatabase")
        {
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.ComplexType<PassStats>();
            modelBuilder.ComplexType<RushStats>();
            modelBuilder.ComplexType<RecStats>();
            modelBuilder.ComplexType<KickReturnStats>();
            modelBuilder.ComplexType<PuntReturnStats>();
            modelBuilder.ComplexType<DefStats>();
            modelBuilder.ComplexType<KickStats>();
            modelBuilder.ComplexType<PuntStats>();

            modelBuilder.Entity<Game>().ToTable("Game");
            modelBuilder.Entity<Game>().HasKey(p => p.GameId);
            modelBuilder.Entity<Game>().Property(p => p.GameId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<TeamStats>().ToTable("TeamStats");
            modelBuilder.Entity<TeamStats>().HasKey(p => new { p.GameId, p.UserId, p.TeamId });

            modelBuilder.Entity<PlayerStats>().ToTable("PlayerStats");
            modelBuilder.Entity<PlayerStats>().HasKey(p => new { p.PlayerId, p.GameId, p.UserId, p.TeamId });

            base.OnModelCreating(modelBuilder);
        }
    }
}