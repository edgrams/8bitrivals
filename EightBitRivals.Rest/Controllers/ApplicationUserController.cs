﻿using System.Linq;
using System.Web.Http;
using EightBitRivals.Domain.Enums;
using EightBitRivals.Domain.Models;
using EightBitRivals.Rest.Contexts;

namespace EightBitRivals.Rest.Controllers
{
    public class ApplicationUserController : ApiController
    {
        /// <summary>
        /// Gets the specified authentication provider.
        /// </summary>
        /// <param name="authProvider">The authentication provider.</param>
        /// <param name="subjectIdentifier">The subject identifier.</param>
        /// <returns></returns>
        public ApplicationUser Get(AuthProviders authProvider, string subjectIdentifier)
        {
            using (var context = new UserContext())
            {
                return context.Users.FirstOrDefault(p => p.AuthProvider == authProvider
                    && p.SubjectIdentifier == subjectIdentifier);
            }
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}