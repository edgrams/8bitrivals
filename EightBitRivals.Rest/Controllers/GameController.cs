﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EightBitRivals.Rest.Contexts;
using TSBDomain.Models;

namespace EightBitRivals.Rest.Controllers
{
    public class GameController : ApiController
    {
        /// <summary>
        /// Gets this instance.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Game> Get()
        {
            using (var context = new GameContext())
            {
                return context.Games.Include("TeamStats.PlayerStats").ToList();
            }
        }

        /// <summary>
        /// Gets the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Game Get(int id)
        {
            using (var context = new GameContext())
            {
                return context.Games.Include("TeamStats.PlayerStats").FirstOrDefault(p => p.GameId == id);
            }
        }

        /// <summary>
        /// Posts the specified game.
        /// </summary>
        /// <param name="game">The game.</param>
        /// <returns></returns>
        public HttpResponseMessage Post(Game game)
        {
            try
            {
                using (var context = new GameContext())
                {
                    context.Games.Add(game);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }

            var response = Request.CreateResponse(HttpStatusCode.Created, game);
            string uri = Url.Link("Default", new { id = game.GameId });
            response.Headers.Location = new Uri(uri);
            return response;
        }

        /// <summary>
        /// Puts the specified game.
        /// </summary>
        /// <param name="game">The game.</param>
        /// <returns></returns>
        public HttpResponseMessage Put(Game game)
        {
            return Request.CreateResponse(HttpStatusCode.NotImplemented);
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                using (var context = new GameContext())
                {
                    var game = context.Games.Include("TeamStats.PlayerStats").FirstOrDefault(p => p.GameId == id);
                    context.Games.Remove(game);
                    context.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, game);
                }
            }          
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
    }
}