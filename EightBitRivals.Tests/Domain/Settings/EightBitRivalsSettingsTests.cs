﻿using EightBitRivals.Domain.Config;
using EightBitRivals.Domain.Settings;
using NUnit.Framework;
using Rhino.Mocks;

namespace EightBitRivals.Tests.Domain.Settings
{
    [TestFixture]
    public class EightBitRivalsSettingsTests
    {
        private EightBitRivalsSettings eightBitRivalsSettings;

        private const string RestUrl = "RestUrl";

        [SetUp]
        public void SetUp()
        {
            this.eightBitRivalsSettings = MockRepository.GeneratePartialMock<EightBitRivalsSettings>();

            this.SetupEightBitRivalsSection();
        }

        private void SetupEightBitRivalsSection()
        {
            var eightBitRivalsSection = new EightBitRivalsSection
            {
                RestUrl = new EightBitRivalsSection.RestUrlElement
                {
                    Value = RestUrl
                }
            };
            this.eightBitRivalsSettings.Stub(p => p.GetConfig()).Return(eightBitRivalsSection);
        }

        [Test]
        public void RestUrl_Should_Return_RestUrl_Value_From_EightBitRivalsSection()
        {
            var result = this.eightBitRivalsSettings.RestUrl;

            Assert.AreEqual(RestUrl, result);
        }
    }
}